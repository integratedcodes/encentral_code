import com.encentral.test_project.commons.exceptions.ResourceNotFoundException;
import com.encentral.test_project.commons.exceptions.UsernameAlreadyExistException;
import com.encentral.test_project.entities.JpaDriver;
import com.encentral.test_project.user_management.impl.DefaultDriverService;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.*;

import java.util.UUID;

import static org.mockito.Mockito.mock;


public class DefaultDriverServiceTest {

    private static Logger logger = LoggerFactory.getLogger(DefaultDriverService.class);

    DefaultDriverService defaultDriverService = mock(DefaultDriverService.class);


    @Test(timeout = 2000)
    public void test_driver_does_not_exist() {
        try {
            JpaDriver jps =
                    defaultDriverService.find("xxx");
        } catch (ResourceNotFoundException resourceNotFoundException) {
            Assert.assertSame("Driver with the id xxx is not found", resourceNotFoundException.getMessage());
        }
    }


    @Test
    public void test_create_new_driver_and_check_result() {
        logger.debug("RUNNING TEST ::: CREATING NEW DRIVER");
        JpaDriver jpaDriver = new JpaDriver();
        jpaDriver.setDriverId(UUID.randomUUID().toString());
        jpaDriver.setUsername("Akeem");
        jpaDriver.setPassword("foobar");
        JpaDriver jpaDriver1 ;

        try {
            jpaDriver1 = defaultDriverService.create(jpaDriver);
            Assert.assertThat(jpaDriver1.getUsername(), sameInstance("Akeem"));
            Assert.assertNull(jpaDriver1.getDriverCar());
            logger.debug("TEST FINISHED ::: Create new driver test finished successfully");
        } catch (UsernameAlreadyExistException usernameAlreadyExistException) {
            usernameAlreadyExistException.printStackTrace();
        }
    }

}
