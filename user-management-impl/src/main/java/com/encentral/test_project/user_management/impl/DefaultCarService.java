package com.encentral.test_project.user_management.impl;

import com.encentral.test_project.commons.exceptions.CarLicencePlateAlreadyExistException;
import com.encentral.test_project.commons.exceptions.ResourceNotFoundException;
import com.encentral.test_project.entities.JpaCar;
import com.encentral.test_project.user_management.api.CarService;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;


@Singleton
public class DefaultCarService implements CarService {

    @Inject
    private JPAApi jpaApi;


    @Override
    public JpaCar find(String carId) throws ResourceNotFoundException {
        JpaCar car = jpaApi.em().find(JpaCar.class, carId.trim());
        if (car == null) {
            throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
        }
        return car;
    }


    @Override
    public boolean delete(String carId) throws ResourceNotFoundException {
        JpaCar jpaCar = find(carId);
        if (jpaCar != null) {
            jpaApi.em().detach(jpaCar);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean updateLicencePlate(String licence_plate, String carId) throws ResourceNotFoundException {
        JpaCar jpaCar;
        if ((jpaCar = jpaApi.em().find(JpaCar.class, licence_plate)) != null) {
            jpaCar.setLicence_plate(licence_plate);
            return true;
        }
        throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
    }


    @Override
    public boolean updateEngineType(String engine_type, String carId) throws ResourceNotFoundException {
        JpaCar jpaCar;
        if ((jpaCar = jpaApi.em().find(JpaCar.class, engine_type)) != null) {
            jpaCar.setEngine_type(engine_type);
            return true;
        }
        throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
    }


    @Override
    public boolean updateRating(Integer rating, String carId) throws ResourceNotFoundException {
        JpaCar jpaCar;
        if ((jpaCar = jpaApi.em().find(JpaCar.class, carId)) != null) {
            jpaCar.setRating(rating);
            return true;
        }
        throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
    }


    @Override
    public boolean updateColor(String color, String carId) throws ResourceNotFoundException {
        JpaCar jpaCar;
        if ((jpaCar = jpaApi.em().find(JpaCar.class, carId)) != null) {
            jpaCar.setColor(color);
            return true;
        }
        throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
    }


    @Override
    public boolean updateStatus(String status, String carId) throws ResourceNotFoundException {
        JpaCar jpaCar;
        if ((jpaCar = jpaApi.em().find(JpaCar.class, carId)) != null) {
            jpaCar.setStatus(status);
            return true;
        }
        throw new ResourceNotFoundException(String.format("Car with id %s not found", carId));
    }


    @Override
    public JpaCar findCarByPlateNumber(String licence_plate) throws ResourceNotFoundException {
        Query query = jpaApi.em().createNativeQuery("SELECT * FROM JpaCar c WHERE c.licence_plate = ? ", JpaCar.class);
        query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
        query.setParameter(1, licence_plate.trim());
        Object result = query.getSingleResult();
        if (result == null) {
            throw new ResourceNotFoundException(String.format("Car with licence_plate %s not found", licence_plate));
        } else {
            return (JpaCar) result;
        }
    }


    @Override
    @SuppressWarnings({"unchecked"})
    public List<JpaCar> findCarByEngineType(@NotNull String carEngineType) throws ResourceNotFoundException {
        Query query = jpaApi.em().createNativeQuery("SELECT * FROM JpaCar c WHERE c.engine_type = ? ", JpaCar.class);
        query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
        query.setParameter(1, carEngineType.trim());
        List<JpaCar> result = query.getResultList();
        if (result == null) {
            throw new ResourceNotFoundException(String.format("Car with licence_plate %s not found", carEngineType));
        } else {
            return result;
        }
    }


    @Override
    public List<JpaCar> findAllCarsSelectedByDrivers(Integer start_search, Integer end_) throws NoResultException {
        Query query = jpaApi.em().createNativeQuery("SELECT * FROM JpaCar c WHERE c.isSelected = TRUE  LIMIT ?,? FOR UPDATE", JpaCar.class);
        query.setParameter(1, start_search);
        query.setParameter(2, end_);
        List<JpaCar> carList = query.getResultList();
        if (carList.size() == 0) {
            throw new NoResultException("No cars could be found...Empty list!");
        } else {
            return carList;
        }
    }


    @Override
    public JpaCar create(@NotNull JpaCar jpaCar) throws CarLicencePlateAlreadyExistException {
        try {
            String licence_plate = jpaCar.getLicence_plate();
            if (!(findCarByPlateNumber(licence_plate) == null)) {
                throw new CarLicencePlateAlreadyExistException("Car licence plate already exists for the new car!");
            }
        } catch (ResourceNotFoundException | NoResultException ex) {
            jpaCar.setId(UUID.randomUUID().toString());
            jpaApi.em().persist(jpaCar);
            return jpaCar;
        }
        return null;
    }

}
