package com.encentral.test_project.user_management.impl;

import com.encentral.test_project.commons.exceptions.*;
import com.encentral.test_project.entities.JpaCar;
import com.encentral.test_project.entities.JpaDriver;
import com.encentral.test_project.user_management.api.DriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PessimisticLockScope;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.*;


@Singleton
public class DefaultDriverService implements DriverService {

    private static final Logger defaultDriverServiceLogger = LoggerFactory.getLogger(DefaultDriverService.class);

    @Inject
    private JPAApi jPAApi;

    @Override
    public JpaDriver find(@NotNull String driverId) throws ResourceNotFoundException {
        Optional<JpaDriver> driver = Optional.ofNullable(jPAApi.em().find(JpaDriver.class, driverId.trim(), LockModeType.PESSIMISTIC_READ));
        if (driver.isPresent()) {
            defaultDriverServiceLogger.info("DRIVER ID FOUND {}", driver.get().getDriverId());
            return driver.get();
        } else throw new ResourceNotFoundException(String.format("Driver with id %s not found", driverId));
    }


    @Override
    public boolean delete(@NotNull String driverId) throws ResourceNotFoundException {
        jPAApi.em().detach(find(driverId));
        return true;
    }

    @Override
    public boolean selectDriverCar(@NotNull String driverId, @NotNull String carId) throws CarAlreadyInUseException, DriverOflineException, ResourceNotFoundException {
        Optional<JpaCar> jpaCarOptional = Optional.ofNullable(jPAApi.em().find(JpaCar.class, carId, LockModeType.PESSIMISTIC_WRITE));
        Optional<JpaDriver> driverOptional = Optional.ofNullable(jPAApi.em().find(JpaDriver.class, driverId, LockModeType.PESSIMISTIC_WRITE));
        if (driverOptional.isPresent() && jpaCarOptional.isPresent()) {
            JpaDriver driver = driverOptional.get();
            JpaCar car = jpaCarOptional.get();
            if (driver.getOnlineStatus().equals("ONLINE")) {
                if (!car.getIsSelected()) {
                    car.setIsSelected(true);
                    driver.setDriverCar(car);
                    return true;
                } else {
                    throw new CarAlreadyInUseException(String.format("Car with id %s has already been selected", carId));
                }
            } else {
                throw new DriverOflineException(String.format("Driver with id %s is OFFLINE and then, not allowed to select a car", driverId));
            }
        } else throw new ResourceNotFoundException("Resource(s) not found. Please provide correct Ids");

    }

    @Override
    public boolean deSelectDriverCar(@NotNull String driverId) throws CarDeselectionException, DriverOflineException, ResourceNotFoundException {
        Map<String, Object> lockerMap = new HashMap<>();
        lockerMap.put("javax.persistence.PessimisticLockScope", PessimisticLockScope.EXTENDED);
        Optional<JpaDriver> driverOptional = Optional.ofNullable(jPAApi.em().find(JpaDriver.class, driverId, lockerMap));
        if (driverOptional.isPresent()) {
            JpaDriver jpaDriver = driverOptional.get();
            if (jpaDriver.getOnlineStatus().equals("ONLINE")) {
                if (jpaDriver.getDriverCar() != null) {
                    jpaDriver.getDriverCar().setIsSelected(false);
                    jpaDriver.setDriverCar(null);
                    return true;
                } else {
                    throw new CarDeselectionException(String.format("Driver with id %s has failed to deselect car", driverId));
                }
            } else {
                throw new DriverOflineException(String.format("Driver with id %s has is OFFLINE and can not deselect assigned car", driverId));
            }
        } else throw new ResourceNotFoundException(String.format("No result found for the driver with id %s ", driverId));
    }

    @Override
    public JpaDriver findByDriverUsername(@NotNull(message = "username can not be empty") String username) throws ResourceNotFoundException, NoResultException {
        Query query = jPAApi.em().createNativeQuery("SELECT * FROM JpaDriver d WHERE d.username = ?", JpaDriver.class);
        query.setParameter(1, username.trim());
        query.setLockMode(LockModeType.PESSIMISTIC_READ);
        Object optionalResult = query.getSingleResult();
        if (!Optional.ofNullable(optionalResult).isPresent()) {
            throw new ResourceNotFoundException(String.format("Driver with username %s not found", username));
        } else {
            return (JpaDriver) optionalResult;
        }
    }

    @Override
    public List<JpaDriver> findByDriverOnlineStatus(@NotNull(message = "Online status can not be null") String onlineStatus) throws ResourceNotFoundException {
        defaultDriverServiceLogger.info("INFO ::: driver online status entered {}", onlineStatus);
        Query query = jPAApi.em().createNativeQuery("SELECT * FROM JpaDriver d WHERE d.online_status = ?", JpaDriver.class);
        query.setLockMode(LockModeType.PESSIMISTIC_READ);
        query.setParameter(1, onlineStatus);
        List<JpaDriver> drivers = query.getResultList();
        if (drivers.size() == 0) {
            throw new ResourceNotFoundException(String.format("Driver with online_status %s not found", onlineStatus));
        }
        return drivers;
    }

    @Override
    public JpaDriver findByDriverCarLicensePlate(@NotNull String licence_plate) throws ResourceNotFoundException, NoResultException {
        Query query = jPAApi.em().createNativeQuery("SELECT * FROM JpaDriver d WHERE d.driverCarId IN (SELECT id FROM JpaCar c GROUP BY c.id HAVING c.licence_plate=?)", JpaDriver.class);
        query.setLockMode(LockModeType.PESSIMISTIC_READ);
        query.setParameter(1, licence_plate);
        Optional<Object> optionalResult = Optional.ofNullable(query.getSingleResult());
        if (!optionalResult.isPresent()) {
            throw new ResourceNotFoundException(String.format("Driver with licence_plate %s not found", licence_plate));
        } else {
            System.out.println(((JpaDriver) optionalResult.get()).getPassword());
            return ((JpaDriver) optionalResult.get());
        }
    }

    @Override
    public List<JpaDriver> findByDriverCarRating(Integer rating) throws ResourceNotFoundException {
        List<JpaDriver> jpaDrivers = new ArrayList<>();
        Query query = jPAApi.em().createNativeQuery("SELECT * FROM JpaDriver d WHERE d.driverCarId IN (SELECT id FROM JpaCar c GROUP BY c.id HAVING c.rating=? ) ", JpaDriver.class);
        query.setLockMode(LockModeType.PESSIMISTIC_READ);
        query.setParameter(1, rating);
        List<JpaDriver> driverList = query.getResultList();
        if (driverList.size() == 0) {
            throw new ResourceNotFoundException(String.format("Driver(s) with car rating %s not found", rating));
        } else {
            for (Object d : driverList) {
                jpaDrivers.add((JpaDriver) d);
            }
            return jpaDrivers;
        }
    }


    @Override
    public JpaDriver create(@NotNull JpaDriver newJpaDriver) throws UsernameAlreadyExistException {
        try {
            String username = newJpaDriver.getUsername();
            if (!(findByDriverUsername(username) == null)) {
                throw new UsernameAlreadyExistException("Username already exists for the new driver");
            }
        } catch (ResourceNotFoundException | NoResultException ex) {
            newJpaDriver.setDateCreated(new Date());
            newJpaDriver.setDriverId(UUID.randomUUID().toString());
            newJpaDriver.setDriverCar(null);
            jPAApi.em().persist(newJpaDriver);
            return newJpaDriver;
        }
        return null;
    }
}
