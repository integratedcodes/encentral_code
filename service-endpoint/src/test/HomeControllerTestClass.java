import controllers.HomeController;
import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import play.test.*;
import play.mvc.*;


public class HomeControllerTestClass {

    @Test
    public void test_home_and_get_content_type(){
        Result result = new HomeController().index();
        Assert.assertEquals(Http.Status.OK, result.status());
        Assert.assertEquals("text/plain", result.contentType().get());
        Assert.assertEquals("utf-8", result.charset().get());
    }


    @Test
    public void test_redirection_location(){
        Result result = new HomeController().index();
        Assert.assertEquals(Http.Status.OK, result.status());
        Assert.assertEquals("", result.redirectLocation().orElse(""));
        Assert.assertEquals("utf-8", result.charset().get());
    }

}
