package controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import play.mvc.Controller;
import play.mvc.Result;


@Api(value = "home")
public class HomeController extends Controller {

    @ApiOperation(value = "Get Index", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = String.class),
            }
    )
    public Result index() {
        try {
            return ok("Correct");
        } catch (Exception ex) {
            return notFound(ex.getMessage());
        }
    }
}

