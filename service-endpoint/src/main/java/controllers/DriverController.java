/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.encentral.test_project.commons.exceptions.*;
import com.encentral.test_project.commons.models.DriverDTO;
import com.encentral.test_project.commons.util.DriverMapper;
import com.encentral.test_project.commons.util.MyObjectMapper;
import com.encentral.test_project.commons.util.ParamValidator;
import com.encentral.test_project.entities.JpaDriver;
import com.encentral.test_project.user_management.impl.DefaultCarService;
import com.encentral.test_project.user_management.impl.DefaultDriverService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;


@Api(value = "Driver")
@Transactional
public class DriverController extends Controller {

    private static final Logger logger = LoggerFactory.getLogger(DefaultCarService.class);
    @Inject
    FormFactory formFactory;
    @Inject
    MyObjectMapper objectMapper;
    @Inject
    DefaultDriverService driverService;

    @ApiOperation(value = "Get Driver", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = DriverDTO.class)
            }
    )
    public Result getDriver(@NotNull(message = "driverId must be provided") String driverId) {
        try {
            ParamValidator.validateParam(driverId);
            return ok(Json.toJson(DriverMapper.jpaDriverToDriverDTO(driverService.find(driverId))));
        }catch(ResourceNotFoundException ex) {
            return notFound(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return internalServerError(ex.getMessage());
        }
    }

    @ApiOperation(value = "Create a Driver", notes = "", httpMethod = "POST")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done")
            }
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "Application",
                            dataType = "com.encentral.test_project.commons.models.DriverDTO",
                            required = true,
                            paramType = "body",
                            value = "Application"
                    )
            }
    )
    public Result createDriver() {
        try {
            Form<DriverDTO> bindFromRequest = formFactory.form(DriverDTO.class).bindFromRequest();
            if (bindFromRequest.hasErrors()) {
                return badRequest(bindFromRequest.errorsAsJson());
            }
            JpaDriver create = driverService.create(DriverMapper.driverDTotoJpaDriver(bindFromRequest.get()));
            return ok(Json.toJson(DriverMapper.jpaDriverToDriverDTO(create)));
        } catch (UsernameAlreadyExistException e) {
            return notFound(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getCause().getMessage());
        }
    }

    @ApiOperation(value = "Delete Driver", notes = "", httpMethod = "DELETE")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done")
            }
    )
    public Result deselectCar(@NotNull(message = "driverId must be provided") String driverId) {
        try {
            ParamValidator.validateParam(driverId);
            driverService.deSelectDriverCar(driverId);
            return ok("Driver with the id supplied deleted successfully");
        }catch (ResourceNotFoundException | NoResultException   ex) {
            return notFound(ex.getMessage());
        } catch (Exception e) {
            return internalServerError("Error 500!");
        }
    }


    @ApiOperation(value = "Get Driver by username", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Request Completed with a success!", response = DriverDTO.class),
                    @ApiResponse(code = 204, message = "No result was found."),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    public Result searchByDriverUsername(@NotNull String username) {
        try {
            ParamValidator.validateParam(username);
            return ok(Json.toJson(DriverMapper.jpaDriverToDriverDTO(driverService.findByDriverUsername(username))));
        } catch (ResourceNotFoundException | NoResultException ex) {
            return notFound(String.format("Driver with username %s not found", username));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }


    @ApiOperation(value = "Get Driver by online status", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = DriverDTO.class)
            }
    )
    public Result searchByDriverOnlineStatus(@NotNull(message = "online status is required") String online_status) {
        try {
            ParamValidator.validateParam(online_status);
            return ok(Json.toJson(DriverMapper.jpaDriverListToDriverDTOList(driverService.findByDriverOnlineStatus(online_status))));
        } catch (ResourceNotFoundException ex) {
            return notFound(ex.getMessage());
        }catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }


    @ApiOperation(value = "Get Driver by car license_plate", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = DriverDTO.class)
            }
    )
    public Result searchByDriverCarLicensePlate(@NotNull(message = "licence plate value is required") String license_plate) {
        try {
            ParamValidator.validateParam(license_plate);
            return ok(Json.toJson(DriverMapper.jpaDriverToDriverDTO(driverService.findByDriverCarLicensePlate(license_plate))));
        } catch (ResourceNotFoundException | NoResultException ex) {
            return notFound(String.format("No driver was found with the provided licence plate number %s", license_plate));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError("Internal server exception occured");
        }
    }

    @ApiOperation(value = "Get Driver by car rating", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = DriverDTO.class)
            }
    )
    public Result searchByDriverCarRating(@NotNull(message = "rating value is required") String rating) {
        try {
            ParamValidator.validateParam(rating);
            return ok(Json.toJson(DriverMapper.jpaDriverListToDriverDTOList(driverService.findByDriverCarRating(Integer.parseInt(rating)))));
        } catch (ResourceNotFoundException ex) {
            return notFound(String.format("drivers with car rating %s not found", rating));
        } catch (Exception e) {
            return internalServerError();
        }

    }


    @ApiOperation(value = "Deselect  a Driver car", notes = "", httpMethod = "DELETE")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = Boolean.class)
            }
    )
    public Result deselectACar(@NotNull(message = "driverId is required") String driverId) {
        try {
            ParamValidator.validateParam(driverId);
            return ok(Json.toJson(driverService.deSelectDriverCar(driverId)));
        } catch (CarDeselectionException | DriverOflineException | NoResultException ex) {
            return notFound(ex.getMessage());
        } catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }

    @ApiOperation(value = "Select a Driver car", notes = "", httpMethod = "POST")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = Boolean.class)
            }
    )
    public Result selectACar(@NotNull(message = "driverId is required") String driverId, @NotNull(message = "carId is required") String carId) {
        try {
            ParamValidator.validateParam(driverId , carId);
            return ok(Json.toJson(driverService.selectDriverCar(driverId, carId)));
        } catch (CarAlreadyInUseException | DriverOflineException | NoResultException ex) {
            return badRequest(ex.getMessage());
        } catch (ResourceNotFoundException resourceNotFoundException) {
            return notFound(resourceNotFoundException.getMessage());
        } catch (Exception e) {
            return internalServerError("Internal server error occurred! Please contact the admin");
        }
    }


    @ApiOperation(value = "Delete a Driver car", notes = "", httpMethod = "POST")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done", response = Boolean.class)
            }
    )
    public Result deleteDriver(@NotNull String driverId) {
        try {
            ParamValidator.validateParam(driverId);
            return ok(Json.toJson(driverService.delete(driverId)));
        } catch (ResourceNotFoundException ex) {
            return notFound(ex.getMessage());
        }catch (Exception ex) {
            return internalServerError(ex.getMessage());
        }
    }
}
