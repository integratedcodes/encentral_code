package controllers;


import com.encentral.test_project.commons.exceptions.CarLicencePlateAlreadyExistException;
import com.encentral.test_project.commons.exceptions.ResourceNotFoundException;
import com.encentral.test_project.commons.models.CarDTO;
import com.encentral.test_project.commons.util.CarMapper;
import com.encentral.test_project.commons.util.ParamValidator;
import com.encentral.test_project.entities.JpaCar;
import com.encentral.test_project.user_management.impl.DefaultCarService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Api(value = "car")
@Transactional
public class CarController extends Controller {


    @Inject
    private FormFactory formFactory;

    @Inject
    private DefaultCarService carService;

    private static Logger logger = LoggerFactory.getLogger(CarController.class);


    @ApiOperation(value = "Get Car", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {

                    @ApiResponse(code = 200, message = "Done", response = CarDTO.class),
                    @ApiResponse(code = 204, message = "No centent", response = CarDTO.class),
            }
    )
    public Result getCar(String carId) {
        try {
            return ok(Json.toJson(CarMapper.carJpaTocCarDTO(carService.find(carId))));
        } catch (ResourceNotFoundException ex) {
            return notFound(ex.getMessage());
        } catch (Exception e) {
            return internalServerError("Internal Server occurred... Please try again! ");
        }
    }


    @ApiOperation(value = "Create a Car", notes = "", httpMethod = "POST")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Done")
            }
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "Application",
                            dataType = "com.encentral.test_project.commons.models.CarDTO",
                            required = true,
                            paramType = "body",
                            value = "Application"
                    )
            }
    )
    public Result createCar() {
        try {
            Form<CarDTO> bindFromRequest = formFactory.form(CarDTO.class).bindFromRequest();
            if (bindFromRequest.hasErrors()) {
                return badRequest(bindFromRequest.errorsAsJson());
            }
            JpaCar create = carService.create(CarMapper.carDTOToJpaCar(bindFromRequest.get()));
            return ok(Json.toJson(CarMapper.carJpaTocCarDTO(create)));
        } catch (CarLicencePlateAlreadyExistException e) {
            e.printStackTrace();
            return notAcceptable(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }


    @ApiOperation(value = "Delete Driver", notes = "", httpMethod = "DELETE")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Car successfully deleted."),
                    @ApiResponse(code = 500, message = "Server failed while trying to process the Request... Try again!")
            }
    )
    public Result deleteCar(@NotNull  String carId) {
        try {
            ParamValidator.validateParam(carId);
            if (carService.delete(carId)) {
                return ok(Json.toJson("Car with carId : " + carId + "has successfully been deleted"));
            } else {
                return notFound("Car with carId " + carId + " could not be deleted because no match was found.");
            }
        } catch (ResourceNotFoundException ex) {
            return notFound(ex.getMessage());
        }catch (Exception ex) {
            return internalServerError(ex.getMessage());
        }
    }




    @ApiOperation(value = "Search a Car by plate number", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Search was completed with status OK!"),
                    @ApiResponse(code = 500, message = "Server Encountered an ERROR"),
            }
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "Application",
                            dataType = "com.encentral.test_project.commons.models.CarDTO",
                            required = true
                    )
            }
    )
    public Result getCarByPlateNumber() {
        try {
            if (request().queryString().containsKey("carPlateNumber")) {
                JpaCar jpaCar = carService.findCarByPlateNumber(request().queryString().get("carPlateNumber")[0]);
                return ok(Json.toJson(CarMapper.carJpaTocCarDTO(jpaCar)));
            } else {
                return badRequest("No 'carPlateNumber' found in the query parameters...");
            }
        } catch (ResourceNotFoundException | NoResultException e) {
            return notFound(String.format("No car found with the particular plate number %s", request().queryString().get("carPlateNumber")[0]));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }


    @ApiOperation(value = "Search car by car Engine type", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Search was completed with status OK!"),
                    @ApiResponse(code = 500, message = "Server Encountered an ERROR"),
            }
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "Application",
                            dataType = "com.encentral.test_project.commons.models.CarDTO[]",
                            required = true
                    )
            }
    )
    public Result getCarByEngineType() {
        try {
            if (request().queryString().containsKey("carEnginetype")) {
                List<JpaCar> carList = carService.findCarByEngineType(request().queryString().get("carEnginetype")[0]);
                return ok(Json.toJson(CarMapper.jpaCarListToCarDTOList(carList)));
            } else {
                return badRequest("No 'carEnginetype' found in the query parameters...");
            }
        } catch (NoResultException e) {
            return noContent();
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }


    @ApiOperation(value = "Search car by car Engine type", notes = "", httpMethod = "GET")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Search was completed with status OK!"),
                    @ApiResponse(code = 500, message = "Server Encountered an ERROR"),
            }
    )
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(
                            name = "Application",
                            dataType = "com.encentral.test_project.commons.models.CarDTO[]",
                            required = true
                    )
            }
    )
    public Result getAllCarsSelectedByDrivers(){
        try {
            Map<String, String[]> queryString = request().queryString();
            List<JpaCar> create = carService.findAllCarsSelectedByDrivers(Integer.parseInt(queryString.get("search_start")[0]), Integer.parseInt(queryString.get("end_")[0]));
            return ok(Json.toJson(CarMapper.jpaCarListToCarDTOList(create)));
        } catch (NoResultException e) {
            e.getMessage();
            return notFound(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }
}
