/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.encentral.test_project.user_management.api;

import com.encentral.test_project.commons.exceptions.*;
import com.encentral.test_project.entities.JpaDriver;

import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *
 * @author James Akinniranye
 */
public interface DriverService {
    
    JpaDriver find(@NotNull String driverId) throws ResourceNotFoundException;

    public JpaDriver create(@NotNull JpaDriver newJpaDriver) throws UsernameAlreadyExistException;

    boolean delete(@NotNull String driverId) throws ResourceNotFoundException;


    boolean selectDriverCar(@NotNull String driverId , String license_plate) throws CarAlreadyInUseException , DriverOflineException  , ResourceNotFoundException;

    public boolean deSelectDriverCar(@NotNull String driverId) throws CarDeselectionException, DriverOflineException, ResourceNotFoundException;

    JpaDriver findByDriverUsername(@NotNull String username) throws ResourceNotFoundException;

    List findByDriverOnlineStatus(@NotNull String online_status) throws ResourceNotFoundException;

    JpaDriver findByDriverCarLicensePlate(@NotNull String licence_plate) throws ResourceNotFoundException, NoResultException ;

    List<JpaDriver> findByDriverCarRating(@NotNull Integer rating) throws Exception ;

    }
