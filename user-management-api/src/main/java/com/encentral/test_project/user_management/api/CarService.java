package com.encentral.test_project.user_management.api;

import com.encentral.test_project.commons.exceptions.CarLicencePlateAlreadyExistException;
import com.encentral.test_project.commons.exceptions.ResourceNotFoundException;
import com.encentral.test_project.entities.JpaCar;

import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;
import java.util.List;


public interface CarService{

    JpaCar find(String driverId) throws ResourceNotFoundException;

    public JpaCar create(@NotNull JpaCar jpaCar) throws CarLicencePlateAlreadyExistException;

    boolean delete(String carId) throws ResourceNotFoundException;

    boolean updateLicencePlate(String licence_plate , String carId) throws ResourceNotFoundException;

    boolean updateEngineType(String engine_type , String carId) throws ResourceNotFoundException;

    boolean updateRating(Integer rating , String carId) throws ResourceNotFoundException;


    boolean updateColor(String color , String carId) throws ResourceNotFoundException;


    boolean updateStatus(String status , String carId ) throws ResourceNotFoundException;

    public JpaCar findCarByPlateNumber(String licence_plate) throws ResourceNotFoundException;

    List<JpaCar> findCarByEngineType(String carEnginetype) throws ResourceNotFoundException;


    public List<JpaCar> findAllCarsSelectedByDrivers(Integer start_search , Integer end) throws ResourceNotFoundException, NoResultException;




    }
