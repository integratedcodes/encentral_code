package com.encentral.test_project.entities;



import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import java.io.Serializable;
import java.util.Date;

@Entity
public class JpaCar implements Serializable{

    @Id
    private String id ;

    @NotNull(message = "licence_plate field can not be null")
    @Column(name = "licence_plate" , unique = true)
    @Size(min = 1 ,  max = 250)
    private String licence_plate ;

    @NotNull(message = "convertible field can not be null")
    @Column(name = "convertible")
    private Boolean convertible ;


    @NotNull(message = "engine_type field can not be null... please provide the value for it")
    @Column(name = "engine_type",nullable = false)
    private String engine_type ;


    @NotNull(message = "rating column field can not be null")
    @Column(name = "rating")
    private Integer rating ;

    @NotNull(message = "seat_count column value can not be null")
    @Column(name = "seat_count")
    private Integer seat_count  ;

    @NotNull(message = "production_year column value can not be null")
    @Column(name = "production_year")
    private String production_year ;

    @Size(min = 1 , max = 250)
    @NotNull(message = "manufacturer column value can not be null")
    @Column(name = "manufacturer")
    private String manufacturer ;

    @NotNull(message = "date_created column value can not be null")
    @Column(name = "date_created")
    private Date dateCreated =  new Date();


    @NotNull(message = "status column value can not be null")
    @Column(name = "status")
    private String status ;

    @NotNull(message = "color column value can not be null")
    @Column(name = "color")
    private String color ;

    @Column(name = "is_selected")
    private boolean isSelected = false;

//    @OneToOne(mappedBy = "driverCar" , fetch = FetchType.EAGER)
//    @JoinColumn(name = "carDriver")
//    private JpaDriver carDriver;

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public String getLicence_plate() {
        return licence_plate;
    }

    public void setLicence_plate(@NotNull String licence_plate) {
        this.licence_plate = licence_plate;
    }

    @NotNull
    public Boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(@NotNull Boolean convertible) {
        this.convertible = convertible;
    }

    @NotNull
    public String getEngine_type() {
        return engine_type;
    }

    public void setEngine_type(@NotNull String engine_type) {
        this.engine_type = engine_type;
    }

    @NotNull
    public Integer getRating() {
        return rating;
    }

    @NotNull
    public void setRating(@NotNull Integer rating) {
        this.rating = rating;
    }

    @NotNull
    public Integer getSeat_count() {
        return seat_count;
    }

    public void setSeat_count(@NotNull Integer seat_count) {
        this.seat_count = seat_count;
    }

    @NotNull
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(@NotNull String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getProduction_year() {
        return production_year;
    }

    public void setProduction_year(@NotNull String production_year) {
        this.production_year = production_year;
    }

    @NotNull
    public Date getDatCreated() {
        return dateCreated;
    }

    public void setDateCreated(@NotNull Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @NotNull
    public Date getDateCreated() {
        return dateCreated;
    }

    @NotNull
    public String getStatus() {
        return status;
    }

    public void setStatus(@NotNull String status) {
        this.status = status;
    }

    @NotNull
    public String getColor() {
        return color;
    }

    public void setColor(@NotNull String color) {
        this.color = color;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

//
//    public JpaDriver getCarDriver() {
//        return carDriver;
//    }
//
//    public void setCarDriver(JpaDriver carDriver) {
//        this.carDriver = carDriver;
//    }

}
