
package com.encentral.test_project.entities;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "JpaDriver.findAll", query = "SELECT j FROM JpaDriver j")})
public class JpaDriver implements Serializable {

    @Id
    @NotNull
    @Column(name = "driver_id")
    private String driverId;


    @NotNull
    @Column(name = "date_created", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;


    @Column(nullable = false, name = "online_status")
    @NotNull(message = "online_status can not be null!")
    @Size(min = 1, max = 10)
    private String onlineStatus;

    @Column(nullable = false)
    @Size(min = 1, max = 255)
    @NotNull(message = "Password can not be null!")
    private String password;


    @Column(nullable = false, unique = true)
    @Size(min = 1, max = 25)
    @NotNull(message = "Username can not be null!")
    private String username;


    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModiied = new Date(0);

    @OneToOne(cascade = CascadeType.PERSIST , fetch = FetchType.EAGER)
    @JoinColumn(name = "driverCarId" , referencedColumnName = "id")
    private JpaCar driverCar;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModiied() {
        return dateModiied;
    }

    public void setDateModiied(Date dateModiied) {
        this.dateModiied = dateModiied;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public JpaCar getDriverCar() {
        return driverCar;
    }

    public void setDriverCar(JpaCar driverCar) {
        this.driverCar = driverCar;
    }
}
