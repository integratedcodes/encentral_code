package com.encentral.test_project.commons.models;


import com.encentral.test_project.entities.JpaCar;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DriverDTO {

    private String driverId = "";

    @NotNull(message = "username can not be null!")
    @Size(min = 10)
    private String username;

    @NotNull(message = "password can not be null!")
    @Size(min = 5)
    @JsonIgnore
    private String password;

    @NotNull(message = "onlineStatus can not be null!")
    private String onlineStatus;

    private Date dateCreated = new Date();

    private Date dateModiied = new Date(0) ;

    private JpaCar driverCar ;


    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModiied() {
        return dateModiied;
    }

    public void setDateModiied(Date dateModiied) {
        this.dateModiied = dateModiied;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public JpaCar getDriverCar() {
        return driverCar;
    }

    public void setDriverCar(JpaCar driverCar) {
        this.driverCar = driverCar;
    }

}
