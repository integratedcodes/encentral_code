package com.encentral.test_project.commons.exceptions;

public class CarLicencePlateAlreadyExistException extends Exception{
    public CarLicencePlateAlreadyExistException(String s) {
        super(s);
    }
}
