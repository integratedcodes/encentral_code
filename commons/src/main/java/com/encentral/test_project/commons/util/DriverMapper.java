
package com.encentral.test_project.commons.util;

import com.encentral.test_project.commons.models.DriverDTO;
import com.encentral.test_project.entities.JpaDriver;
import java.util.ArrayList;
import java.util.List;

public class DriverMapper {

    public static DriverDTO jpaDriverToDriverDTO(JpaDriver jpaDriver) {
        DriverDTO dTO = new DriverDTO();
        dTO.setDateCreated(jpaDriver.getDateCreated());
        dTO.setDateModiied(jpaDriver.getDateModiied());
        dTO.setDriverId(jpaDriver.getDriverId());
        dTO.setUsername(jpaDriver.getUsername());
        dTO.setOnlineStatus(jpaDriver.getOnlineStatus());
        dTO.setDriverCar(jpaDriver.getDriverCar());
        return dTO;
    }

    public static JpaDriver driverDTotoJpaDriver(DriverDTO dTO) {
        JpaDriver jpaDriver = new JpaDriver();
        jpaDriver.setDateCreated(dTO.getDateCreated());
        jpaDriver.setDateModiied(dTO.getDateModiied());
        jpaDriver.setDriverId(dTO.getDriverId());
        jpaDriver.setUsername(dTO.getUsername());
        jpaDriver.setPassword(dTO.getPassword());
        jpaDriver.setOnlineStatus(dTO.getOnlineStatus());
        return jpaDriver;
    }

    public static List<DriverDTO> jpaDriverListToDriverDTOList(List jpaDriver) {
        List<DriverDTO> driverDTOS = new ArrayList<>();
        for (Object driver : jpaDriver) {
            driverDTOS.add(jpaDriverToDriverDTO((JpaDriver) driver));
        }
        return driverDTOS;
    }

    public static List<JpaDriver> dtoDriverListToJpaDriverList(List<DriverDTO> driverDTOS) {
        List<JpaDriver> jpaDrivers = new ArrayList<>();
        for (DriverDTO driverDTO : driverDTOS) {
            jpaDrivers.add(driverDTotoJpaDriver(driverDTO));
        }
        return jpaDrivers;
    }

}
