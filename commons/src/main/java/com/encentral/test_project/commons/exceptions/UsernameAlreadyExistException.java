package com.encentral.test_project.commons.exceptions;

public class UsernameAlreadyExistException extends Exception {
    public UsernameAlreadyExistException(String message){
        super(message);
    }
}
