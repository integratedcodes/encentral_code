package com.encentral.test_project.commons.util;

import com.encentral.test_project.commons.exceptions.ResourceNotFoundException;

import javax.validation.constraints.NotNull;

public class ParamValidator {

    public static void validateParam(@NotNull String c) throws ResourceNotFoundException {
        if (c.isEmpty()) {
            throw new ResourceNotFoundException("No result found!");
        }
    }


    public static void validateParam(@NotNull String c ,@NotNull String d ) throws ResourceNotFoundException {
        if (c.isEmpty() || d.isEmpty()) {
            throw new ResourceNotFoundException("No result found!");
        }
    }

}
