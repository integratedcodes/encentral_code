package com.encentral.test_project.commons.exceptions;

public class CarDeselectionException extends Exception {

    public CarDeselectionException(String message) {
        super(message);
    }

}
