/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.encentral.test_project.commons.exceptions;

/**
 *
 * @author Akinniranye James Ayodele <kaimedavies@sycliff.com>
 */
public class ResourceAlreadyExistException extends Exception {

    public ResourceAlreadyExistException(String message) {
        super(message);
    }

}
