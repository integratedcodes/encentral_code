package com.encentral.test_project.commons.exceptions;

public class DriverOflineException extends Exception {
    public DriverOflineException(String message){
        super(message);
    }
}
