package com.encentral.test_project.commons.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDTO {

    private String id = "";

    @NotNull(message = "licence_plate field can not be null")
    @Size(min = 1 ,  max = 250)
    private String licence_plate ;

    @NotNull(message = "convertible field can not be null")
    private boolean convertible ;


    @NotNull(message = "engine_type field can not be null... please provide the value for it")
    @Size(min = 1 ,  max = 250)
    private String engine_type ;

    @NotNull(message = "rating column field can not be null")
    private Integer rating ;


    @NotNull(message = "seat_count column value can not be null")
    private Integer seat_count  ;

    @NotNull(message = "production_year column value can not be null")
    private String  production_year ;


    @NotNull(message = "manufacturer field can not be null")
    @Size(min = 1 ,  max = 250)
    private String manufacturer ;

    private Date dateCreated =  new Date();

    @NotNull(message = "color column value can not be null")
    @Size(min = 1 ,  max = 250)
    private String color  ;

    @NotNull(message = "status column value can not be null")
    @Size(min = 1 ,  max = 250)
    private String status  ;

    private boolean isSelected = false;


    @NotNull
    public String getLicence_plate() {
        return licence_plate;
    }

    public void setLicence_plate(@NotNull String licence_plate) {
        this.licence_plate = licence_plate;
    }

    @NotNull
    public boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(@NotNull boolean convertible) {
        this.convertible = convertible;
    }

    @NotNull
    public String getEngine_type() {
        return engine_type;
    }

    public void setEngine_type(@NotNull String engine_type) {
        this.engine_type = engine_type;
    }


    @NotNull
    public Integer getSeat_count() {
        return seat_count;
    }

    public void setSeat_count(@NotNull Integer seat_count) {
        this.seat_count = seat_count;
    }

    @NotNull
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(@NotNull String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isConvertible() {
        return convertible;
    }

    @NotNull
    public String getColor() {
        return color;
    }

    public void setColor(@NotNull String color) {
        this.color = color;
    }

    @NotNull
    public String getStatus() {
        return status;
    }

    public void setStatus(@NotNull String status) {
        this.status = status;
    }

    @NotNull
    public String getCarStatus() {
        return status;
    }

    public void setCarStatus(@NotNull String carStatus) {
        this.status = carStatus;
    }

    @NotNull
    public Date getDateCreated() {
        return dateCreated;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public Integer getRating() {
        return rating;
    }

    public void setRating(@NotNull Integer rating) {
        this.rating = rating;
    }

    @NotNull
    public String getProduction_year() {
        return production_year;
    }

    public void setProduction_year(@NotNull String production_year) {
        this.production_year = production_year;
    }

    public void setDateCreated(@NotNull Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
