package com.encentral.test_project.commons.util;

import com.encentral.test_project.commons.models.CarDTO;
import com.encentral.test_project.entities.JpaCar;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CarMapper {

    public  static  JpaCar carDTOToJpaCar(@NotNull CarDTO carDTO){

        JpaCar jpaCar = new JpaCar();
        jpaCar.setSeat_count(carDTO.getSeat_count());
        jpaCar.setDateCreated(carDTO.getDateCreated());
        jpaCar.setId(carDTO.getId());
        jpaCar.setColor(carDTO.getColor());
        jpaCar.setEngine_type(carDTO.getEngine_type());
        jpaCar.setRating(carDTO.getRating());
        jpaCar.setProduction_year(carDTO.getProduction_year());
        jpaCar.setManufacturer(carDTO.getManufacturer());
        jpaCar.setStatus(carDTO.getStatus());
        jpaCar.setConvertible(carDTO.getConvertible());
        jpaCar.setLicence_plate(carDTO.getLicence_plate());
        jpaCar.setIsSelected(carDTO.isSelected());

        return jpaCar ;
    }


    public static CarDTO carJpaTocCarDTO(@NotNull JpaCar jpaCar){
        CarDTO dTO = new CarDTO();
        dTO.setSeat_count(jpaCar.getSeat_count());
        dTO.setDateCreated(jpaCar.getDateCreated());
        dTO.setId(jpaCar.getId());
        dTO.setColor(jpaCar.getColor());
        dTO.setEngine_type(jpaCar.getEngine_type());
        dTO.setRating(jpaCar.getRating());
        dTO.setProduction_year(jpaCar.getProduction_year());
        dTO.setManufacturer(jpaCar.getManufacturer());
        dTO.setStatus(jpaCar.getStatus());
        dTO.setConvertible(jpaCar.getConvertible());
        dTO.setLicence_plate(jpaCar.getLicence_plate());
        dTO.setSelected(jpaCar.getIsSelected());
        return dTO ;
    }

    /**
     *
     * @param jpaCars The list of JpaCars
     * @return List<CarDTO>
     */
    public static List<CarDTO> jpaCarListToCarDTOList(List<JpaCar> jpaCars) {
        List<CarDTO> carDTOS = new ArrayList<>();
        for (JpaCar driver : jpaCars) {
            carDTOS.add(carJpaTocCarDTO(driver));
        }
        return carDTOS;
    }

    /**
     *
     * @param carDTOS List of CarDTOs to be converted to List<JpaCar>
     * @return List<JpaCar>
     */
    public static List<JpaCar> dtoCarListToJpaCarList(List<CarDTO> carDTOS) {
        List<JpaCar> jpaCars = new ArrayList<>();
        for (CarDTO carDTO : carDTOS) {
            jpaCars.add(carDTOToJpaCar(carDTO));
        }
        return jpaCars;
    }
}
